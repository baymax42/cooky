import Datastore from 'nedb'
import path from 'path'
import { remote } from 'electron'

var db = new Datastore({
  autoload: true,
  filename: path.join(remote.app.getPath('userData'), '/recipes.db'),
  timestampData: true
})

export default db
