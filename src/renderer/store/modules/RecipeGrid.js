const state = {
  menu: [
    {icon: 'view_list', title: 'Recipes', color: {shade: 'red', type: 'accent', value: 3}, to: '/'},
    {icon: 'view_list', title: 'A', color: {shade: 'yellow', type: 'darken', value: 1}, to: '/a'},
    {icon: 'view_list', title: 'B', color: {shade: 'green', type: 'accent', value: 4}, to: '/a'},
    {icon: 'view_list', title: 'C', color: {shade: 'purple', type: 'accent', value: 3}, to: '/a'},
    {icon: 'view_list', title: 'About', color: {shade: 'cyan', type: 'accent', value: 3}, to: '/b'}
  ]
}

const getters = {
  getMenu (state) {
    return state.menu
  }
}

export default {
  state,
  getters
}
