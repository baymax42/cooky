import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'recipe-grid',
      component: require('@/components/RecipeGrid').default
    },
    {
      path: '/add/',
      name: 'recipe-add',
      component: require('@/components/RecipeGrid/RecipeAdd').default
    },
    {
      path: '/view/:id',
      name: 'recipe',
      component: require('@/components/RecipeGrid/RecipeView').default,
      props: true
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
